const initState = {
  value: 0,
  isLoading: false,
};

function reducer(state = initState, action) {
  switch (action.type) {
    case 'SUCCES_ADD': {
      return {
        ...state,
        value: action.payload.random,
        isLoading: false,
      };
    }
    default:
      return state;
  }
}

export default reducer;
