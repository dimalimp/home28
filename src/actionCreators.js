export function addCount(num) {
  return {
    type: 'NUM_ADD',
    payload: {
      num,
    },
  };
}
