import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCount } from '../../actionCreators';

class Button extends Component {
    render() {
        const { isLoading, add } = this.props;
        return (
            <button disabled={ isLoading } onClick={ () => add('Home') }>Случайное число-
                { this.props.value }
            </button>
        );
    }
}

const mapStateToProps = ({ count }) => ({
    value: count.value,
    isLoading: count.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
    add: (num) => dispatch(addCount(num)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Button);