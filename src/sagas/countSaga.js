import { put, takeEvery } from 'redux-saga/effects';

const delay = (ms) => new Promise((res) => setTimeout(res, ms))

export function* incrementAsync() {
  const random = Math.floor(Math.random() * 10)
  yield delay(2000)
  yield put({
    type: 'SUCCES_ADD',
    payload: {
      random
    }
  })
}

export default function* countSaga() {
  yield takeEvery('NUM_ADD', incrementAsync);
}
