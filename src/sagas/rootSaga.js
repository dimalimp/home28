import { all } from 'redux-saga/effects';
import countSaga from './countSaga';

export default function* rootSaga() {
  yield all([countSaga()]);
}
